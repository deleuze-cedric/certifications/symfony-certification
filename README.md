# Symfony certification

## Local

1) Démarrer votre infra (traefik, malhog, mysql). \
Si pas créer voir https://gitlab.com/deleuze_cedric/docker-web-pack/-/tree/master/web-pack


2) Exécuter le fichier **generate_dev_env.sh**\
`sh ./generate_dev_env.sh` et compléter le fichier **.env** généré\
**OU** dupliquer le **.env.sample** à la racine et le renommer **.env** puis compléter le


3) Dupliquer le **.env.sample** dans le répertoire **symfony** et le renommer **.env** puis compléter le


4) Editer votre fichier hosts (en mode super utilisateur)
    - Linux/Mac : **/etc/hosts**
    - Windows : **C:/windows/system32/drivers/etc/hosts**


5) Ajouter, si elles n'existent pas déjà, les lignes suivantes :\
   Remplacer 127.0.0.1 par l'ip de votre traefik si il est appelable depuis une autre IP
    - 127.0.0.1 local.symfony-certification.com
    - 127.0.0.1 local.mercure.symfony-certification.com

    
6) Démarrer les containers de l'api  : `docker compose up`


7) Connecter vous au shell du container de l'api symfony :\
`docker compose exec -it symfony_certification bash`


8) Installer le vendor : `composer install`


9) Mettre à jour la db : `symfony console d:d:c --if-not-exists; symfony console d:m:m -q;`


10) Se déconnecter du container symfony pour se connecter au docker node :\
`docker compose exec -it symfony_certification_node bash`


11) Télécharger les dépendances :\
`yarn install`


11) Lancer le watch (tailwind et encore) :\
`yarn watch`


12) Lancer les fixtures :\
`php -d memory_limit=500M bin/console h:f:l -q` 

# Staging

https://symfony-certification.deleuze-cedric.com

# Prod
