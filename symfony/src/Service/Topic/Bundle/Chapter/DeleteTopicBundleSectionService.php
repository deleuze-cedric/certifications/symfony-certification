<?php

namespace App\Service\Topic\Bundle\Chapter;

use App\Entity\Topic\Topic;
use App\Service\Topic\SlugifyTopicNameService;
use App\Utils\DeleteDirService;

final readonly class DeleteTopicBundleSectionService
{
    public function __construct(
        private string                  $topicBundleDir,
        private DeleteDirService        $removeDirService,
        private SlugifyTopicNameService $slugifyTopicNameService,
    ) {}

    public function execute(Topic $chapter): void
    {
        $bundleName = $chapter->getParent()?->getBundleName();
        $chapterName = $chapter->getName();
        $bundlePath = $this->topicBundleDir . '/' . $bundleName . '/src/' . $this->slugifyTopicNameService->execute($chapterName);
        $this->removeDirService->execute($bundlePath);
    }
}
