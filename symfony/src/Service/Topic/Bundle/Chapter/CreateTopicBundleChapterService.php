<?php

namespace App\Service\Topic\Bundle\Chapter;

use App\Entity\Topic\Topic;
use App\Service\Topic\SlugifyTopicNameService;
use App\Utils\CreateFileService;

final readonly class CreateTopicBundleChapterService
{
    public function __construct(
        private SlugifyTopicNameService $slugifyTopicNameService,
        private string                  $topicBundleDir,
        private string                  $gitignoreFilename,
        private CreateFileService       $createFileService,
    ) {}

    public function execute(Topic $chapter): void
    {
        $topicBundleName = $chapter->getParent()?->getBundleName();
        $chapterName = $chapter->getName();
        $chapterPath = $this->topicBundleDir . '/' . $topicBundleName . '/src/' . $this->slugifyTopicNameService->execute($chapterName);
        $gitignorePath = $chapterPath . '/' . $this->gitignoreFilename;

        if (!file_exists($gitignorePath)) {
            $this->createFileService->execute($gitignorePath, '');
        }
    }
}
