<?php

namespace App\Service\Topic\Bundle\Chapter;

use App\Entity\Topic\Topic;
use App\Utils\DeleteDirService;
use App\Utils\MergeDirService;

final readonly class MoveTopicBundleChapterService
{
    public function __construct(
        private string           $topicBundleDir,
        private DeleteDirService $deleteDirService,
        private MergeDirService  $mergeDirService,
    ) {}

    public function execute(Topic $chapter, string $oldBundleName, string $oldChapterName): void
    {
        $newBundleName = $chapter->getParent()?->getBundleName();
        $chapterName = $chapter->getName();

        $newChapterPath = $this->topicBundleDir . '/' . $newBundleName . '/src/' . $chapterName;
        $oldChapterPath = $this->topicBundleDir . '/' . $oldBundleName . '/src/' . $oldChapterName;

        if (file_exists($newChapterPath)) {
            $this->mergeDirService->execute($oldChapterPath, $newChapterPath);
            $this->deleteDirService->execute($oldChapterPath);
        } else {
            rename($oldChapterPath, $newChapterPath);
        }
    }
}
