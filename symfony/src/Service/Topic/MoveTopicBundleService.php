<?php

namespace App\Service\Topic;

use App\Entity\Topic\Topic;
use App\Utils\CreateFileService;
use App\Utils\DeleteDirService;
use App\Utils\MergeDirService;
use DirectoryIterator;

final readonly class MoveTopicBundleService
{
    public function __construct(
        private string                   $topicBundleDir,
        private CreateTopicBundleService $createTopicBundleService,
        private DeleteDirService         $deleteDirService,
        private MergeDirService          $mergeDirService,
    ) {}

    public function execute(Topic $topic, string $oldBundleName): void
    {
        $newBundleName = $topic->getBundleName();
        $newBundlePath = $this->topicBundleDir . '/' . $newBundleName;
        $oldBundlePath = $this->topicBundleDir . '/' . $oldBundleName;

        $dirsToMove = ['src', 'tests'];

        if (file_exists($newBundlePath)) {
            foreach ($dirsToMove as $dir) {
                $this->mergeDirService->execute(
                    $oldBundleName . '/' . $dir,
                    $newBundleName . '/' . $dir
                );
            }
            $this->deleteDirService->execute($oldBundlePath);
        } else {
            $this->createTopicBundleService->execute($topic);
            foreach ($dirsToMove as $dir) {
                rename($oldBundlePath . '/' . $dir, $newBundlePath . '/' . $dir);
            }
        }
    }
}
