<?php

namespace App\Service\Topic;

use Symfony\Component\String\Slugger\SluggerInterface;

final readonly class SlugifyTopicNameService
{
    public function __construct(
        private SluggerInterface $slugger,
    ) {}

    public function execute(string $topicName): string
    {
        return $this->slugger->slug(ucwords($topicName), '');
    }
}
