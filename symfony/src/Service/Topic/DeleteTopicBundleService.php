<?php

namespace App\Service\Topic;

use App\Entity\Topic\Topic;
use App\Utils\CreateFileService;
use App\Utils\DeleteDirService;

final readonly class DeleteTopicBundleService
{
    public function __construct(
        private string           $topicBundleDir,
        private DeleteDirService $removeDirService,
    ) {}

    public function execute(Topic $topic): void
    {
        $bundleName = $topic->getBundleName();
        $bundlePath = $this->topicBundleDir . '/' . $bundleName;
        $this->removeDirService->execute($bundlePath);
    }
}
