<?php

namespace App\Service\Topic;

use App\Entity\Topic\Topic;
use App\Utils\CreateFileService;

final readonly class CreateTopicBundleService
{
    public function __construct(
        private string            $topicBundleDir,
        private string            $topicBundleNamespacePrefix,
        private string            $gitignoreFilename,
        private CreateFileService $createFileService,
    ) {}

    public function execute(Topic $topic): void
    {
        $bundleName = $topic->getBundleName();
        $bundlePath = $this->topicBundleDir . '/' . $bundleName;

        $namespace = $this->topicBundleNamespacePrefix . '\\' . $bundleName;
        $composerNamespace = str_replace('\\', '\\\\', $namespace);

        $bundleClass = <<<EOF
            <?php
            
            namespace $namespace;
            
            use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
            
            class $bundleName extends AbstractBundle {}
            
            EOF;

        $this->createFileService->execute($bundlePath . '/' . $bundleName . '.php', $bundleClass);

        $bundleComposer = <<<EOF
            {
                "autoload": {
                    "psr-4": {
                        "$composerNamespace\\\\": "src/"
                    }
                },
                "autoload-dev": {
                    "psr-4": {
                        "$composerNamespace\\\\Tests\\\\": "tests/"
                    }
                }
            }
            EOF;

        $this->createFileService->execute($bundlePath . '/composer.json', $bundleComposer);
        $this->createFileService->execute($bundlePath . '/src/' . $this->gitignoreFilename, '');
        $this->createFileService->execute($bundlePath . '/tests/' . $this->gitignoreFilename, '');
    }
}
