<?php

namespace App\Utils;

use App\Interface\FixturableEntityInterface;
use SplFileObject;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Yaml\Yaml;

final readonly class AliceFixtureGenerator
{
    public const GROUP = 'yaml';

    public function __construct(
        /** @var Serializer $serializer */
        private SerializerInterface $serializer,
        private CreateFileService   $createFileService,
    ) {}

    /**
     * @param string $filePath
     * @param array<string> $fileToInclude
     * @return SplFileObject
     */
    public function initYamlFile(string $filePath, array $fileToInclude): SplFileObject
    {
        /** @var SplFileObject $file */
        $file = $this->createFileService->execute(filename: $filePath, returnFile: true, mode: 'w');

        if (count($fileToInclude) > 0) {
            $this->writeYaml($file, ['include' => $fileToInclude]);
        }

        return $file;
    }

    /**
     * @param FixturableEntityInterface $entity
     * @return array<array|mixed>
     * @throws ExceptionInterface
     */
    public function normalizeEntity(FixturableEntityInterface $entity): array
    {
        /** @var array<array|mixed> $array */
        $array = $this->serializer->normalize($entity, null, ['groups' => self::GROUP]);
        return $array;
    }

    /**
     * @param SplFileObject $fileObject
     * @param string $entityClass
     * @param array<array|mixed> $entities
     * @return void
     */
    public function writeEntities(SplFileObject $fileObject, string $entityClass, array $entities): void
    {
        $this->writeYaml($fileObject, [$entityClass => $entities]);
    }

    /**
     * @param SplFileObject $fileObject
     * @param array<array|mixed> $data
     * @return void
     */
    public function writeYaml(SplFileObject $fileObject, array $data): void
    {
        $fileObject->fwrite(Yaml::dump($data, Yaml::DUMP_OBJECT_AS_MAP));
    }
}
