<?php

namespace App\Utils;

use SplFileObject;

class CreateFileService
{
    public function execute(string $filename, ?string $content = null, int $permissions = 0o766, bool $returnFile = false, string $mode = 'c+'): ?SplFileObject
    {
        $dir = dirname($filename);

        if (!file_exists($dir)) {
            mkdir($dir, $permissions, true);
        }

        $file = new SplFileObject($filename, $mode);

        if ($content) {
            $file->fwrite($content);
        }

        if (!$returnFile) {
            unset($file);
            return null;
        }

        return $file;
    }

}
