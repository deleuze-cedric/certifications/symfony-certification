<?php

namespace App\Utils;

use DirectoryIterator;

class DeleteDirService
{
    public function execute(string $dir): void
    {
        $iterator = new DirectoryIterator($dir);

        /** @var DirectoryIterator $file */
        foreach ($iterator as $file) {
            if ($file->isDot()) {
                continue;
            }

            $filePath = $file->getPathname();

            if ($file->isDir()) {
                $this->execute($filePath);
                rmdir($filePath);
            } elseif ($file->isFile()) {
                unlink($filePath);
            }
        }
    }
}
