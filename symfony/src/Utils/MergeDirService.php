<?php

namespace App\Utils;

use DirectoryIterator;

class MergeDirService
{
    public function execute(string $dir, string $newDir): void
    {
        $iterator = new DirectoryIterator($dir);

        /** @var DirectoryIterator $file */
        foreach ($iterator as $file) {
            if ($file->isDot()) {
                continue;
            }

            $filePath = $file->getPath();
            $fileName = $file->getFilename();
            $newFilePath = $newDir . '/' . $fileName;

            if ($file->isDir()) {
                if (file_exists($newFilePath)) {
                    $this->execute($filePath, $newFilePath);
                } else {
                    rename($filePath, $newFilePath);
                }
            } else {
                if (!file_exists($newFilePath)) {
                    rename($filePath, $newFilePath);
                }
            }
        }
    }
}
