<?php

namespace App;

use DirectoryIterator;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait {
        registerBundles as private traitRegisterBundles;
    }

    /**
     * @return iterable<object>
     */
    public function registerBundles(): iterable
    {
        yield from $this->traitRegisterBundles();

        if ("dev" !== $this->environment) {
            return;
        }

        $topicsDir = $this->getTopicBundleDir();
        $topicsNamespacePrefix = $this->getTopicBundleNamespacePrefix();

        if (file_exists($topicsDir)) {
            $iterator = new DirectoryIterator($topicsDir);

            /** @var DirectoryIterator $bundleDir */
            foreach ($iterator as $bundleDir) {
                if ($bundleDir->isDot()) {
                    continue;
                }

                if (!$bundleDir->isDir()) {
                    continue;
                }

                $class = $topicsNamespacePrefix . "\\$bundleDir\\$bundleDir";
                yield new $class();
            }
        }
    }

    protected function buildContainer(): ContainerBuilder
    {
        $container = parent::buildContainer();

        $topicsDir = $this->getTopicBundleDir();
        $topicsNamespacePrefix = $this->getTopicBundleNamespacePrefix();

        $container->setParameter('topic.bundle.dir', $topicsDir);
        $container->setParameter('topic.bundle.namespace.prefix', $topicsNamespacePrefix);

        return $container;
    }

    private function getTopicBundleDir(): string
    {
        $kernelParameters = $this->getKernelParameters();
        $kernelProjectDir = $kernelParameters['kernel.project_dir'];
        return $kernelProjectDir . '/src/Topics';
    }

    private function getTopicBundleNamespacePrefix(): string
    {
        return 'App\\Topics';
    }
}
