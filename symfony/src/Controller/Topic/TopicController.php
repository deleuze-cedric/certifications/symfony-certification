<?php

namespace App\Controller\Topic;

use App\Entity\Topic\Topic;
use App\Entity\User;
use App\Form\Topic\TopicType;
use App\Repository\Topic\TopicRepository;
use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/topics', 'app_topic_')]
final class TopicController extends AbstractController
{
    /** @var NestedTreeRepository<Topic> $topicNestedTreeRepository */
    private readonly NestedTreeRepository $topicNestedTreeRepository;

    public function __construct(
        private readonly TopicRepository        $topicRepository,
        private readonly EntityManagerInterface $entityManager,
    ) {
        /** @var NestedTreeRepository<Topic> $nestedTreeRepository */
        $nestedTreeRepository = $this->entityManager->getRepository(Topic::class);
        $this->topicNestedTreeRepository = $nestedTreeRepository;
    }

    #[Route('/', name: 'list', methods: ['GET'])]
    public function list(): Response
    {
        return $this->getListResponse();
    }

    #[Route('/{slug}', name: 'show', methods: ['GET'])]
    public function show(Topic $topic): Response
    {
        return $this->getListResponse($topic);
    }

    private function getListResponse(?Topic $topic = null): Response
    {
        $topics = $this->topicRepository->findBy(['parent' => $topic]);
        $nbChildren = $this->countChild($topics);
        $breadcrumb = [];

        if ($topic) {
            foreach ($topic->getTopicPath() as $parentTopic) {
                $topicUrl = $this->generateUrl('app_topic_show', ['slug' => $parentTopic->getSlug()]);
                $breadcrumb = [
                    ...$breadcrumb,
                    $topicUrl => $parentTopic->getName(),
                ];
            }
        }

        return $this->render('topic/index.html.twig', [
            'nbChildren' => $nbChildren,
            'topic' => $topic,
            'topics' => $topics,
            'breadcrumb' => $breadcrumb,
        ]);
    }

    /**
     * @param array<Topic> $topics
     * @return array<int, int>
     */
    private function countChild(array $topics): array
    {
        $nbChildren = [];

        foreach ($topics as $topic) {
            $nbChildren[(int)$topic->getId()] = $this->topicNestedTreeRepository->childCount($topic);
        }

        return $nbChildren;
    }

    #[IsGranted(User::ROLE_USER)]
    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $topic = new Topic();
        $form = $this->createForm(TopicType::class, $topic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($topic);
            $entityManager->flush();

            return $this->redirectToRoute('app_topic_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('topic/new.html.twig', [
            'topic' => $topic,
            'form' => $form,
        ]);
    }

    #[IsGranted(User::ROLE_USER)]
    #[Route('/{slug}/edit', name: 'edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Topic $topic, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TopicType::class, $topic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_topic_list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('topic/edit.html.twig', [
            'topic' => $topic,
            'form' => $form,
        ]);
    }

    #[IsGranted(User::ROLE_ADMIN)]
    #[Route('/{slug}', name: 'delete', methods: ['DELETE'])]
    public function delete(Request $request, Topic $topic, EntityManagerInterface $entityManager): Response
    {
        /** @var string $token */
        $token = $request->request->get('_csrf_token');

        if ($this->isCsrfTokenValid('delete' . $topic->getId(), $token)) {
            $entityManager->remove($topic);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_topic_list', [], Response::HTTP_SEE_OTHER);
    }
}
