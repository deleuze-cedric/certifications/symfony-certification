<?php

namespace App\Controller\Admin\Topic;

use App\Entity\Topic\Topic;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TopicCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Topic::class;
    }
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            TextField::new('name')
                ->setLabel('Nom'),
            TextareaField::new('description')
                ->setLabel('Description'),
            AssociationField::new('parent')
                ->setLabel('Parent')
                ->setCrudController(TopicCrudController::class),
            DateTimeField::new('createdAt')
                ->setLabel('Créer le')
                ->hideOnForm(),
        ];
    }
}
