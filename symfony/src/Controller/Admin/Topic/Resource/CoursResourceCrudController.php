<?php

namespace App\Controller\Admin\Topic\Resource;

use App\Entity\Resource\CoursResource;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CoursResourceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CoursResource::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
