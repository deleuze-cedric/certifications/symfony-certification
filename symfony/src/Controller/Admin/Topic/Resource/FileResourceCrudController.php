<?php

namespace App\Controller\Admin\Topic\Resource;

use App\Entity\Resource\FileResource;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class FileResourceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return FileResource::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
