<?php

namespace App\Controller\Admin\Topic\Resource;

use App\Entity\Resource\UrlResource;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class UrlResourceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UrlResource::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            UrlField::new('url'),
        ];
    }
}
