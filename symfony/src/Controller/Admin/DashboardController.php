<?php

namespace App\Controller\Admin;

use App\Entity\Resource\CoursResource;
use App\Entity\Resource\FileResource;
use App\Entity\Resource\UrlResource;
use App\Entity\Test\Answer;
use App\Entity\Test\Question;
use App\Entity\Topic\Topic;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\Menu\CrudMenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'app_admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            // the name visible to end users
            ->setTitle('Dashboard')
            // you can include HTML contents too (e.g. to link to an image)
            ->setTitle('Dashboard')

            // there's no need to define the "text direction" explicitly because
            // its default value is inferred dynamically from the user locale
            ->setTextDirection('ltr')

            // set this option if you prefer the page content to span the entire
            // browser width, instead of the default design which sets a max width
            ->renderContentMaximized()

            // set this option if you prefer the sidebar (which contains the main menu)
            // to be displayed as a narrow column instead of the default expanded design
            ->renderSidebarMinimized()

            // by default, all backend URLs are generated as absolute URLs. If you
            // need to generate relative URLs instead, call this method
            ->generateRelativeUrls();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::subMenu('Test', 'fa fa-vial-virus')->setSubItems(
            iterator_to_array(
                $this->configureTestMenu()
            )
        );

        yield MenuItem::subMenu('Topic', 'fa fa-sitemap')->setSubItems(
            iterator_to_array(
                $this->configureTopicMenu()
            )
        );
    }

    /**
     * @return \Traversable<CrudMenuItem>
     */
    private function configureTestMenu(): \Traversable
    {
        yield MenuItem::linkToCrud('Question', 'fas fa-question', Question::class);
        yield MenuItem::linkToCrud('Réponse', 'fas fa-square-check', Answer::class);
    }

    /**
     * @return \Traversable<CrudMenuItem>
     */
    private function configureTopicMenu(): \Traversable
    {
        yield MenuItem::linkToCrud('Sujet', 'fas fa-question', Topic::class);
        yield MenuItem::linkToCrud('Cours', 'fas fa-pen', CoursResource::class);
        yield MenuItem::linkToCrud('URL', 'fas fa-link', UrlResource::class);
        yield MenuItem::linkToCrud('Fichier', 'fas fa-file', FileResource::class);
    }
}
