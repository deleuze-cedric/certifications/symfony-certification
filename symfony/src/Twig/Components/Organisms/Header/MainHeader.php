<?php

namespace App\Twig\Components\Organisms\Header;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(template: '@Organisms/Header/MainHeader.html.twig')]
class MainHeader
{
    private ?string $activeRoute;

    public function __construct(
        RequestStack                           $requestStack,
        private readonly UrlGeneratorInterface $generator
    ) {
        $this->activeRoute = $requestStack->getCurrentRequest()?->attributes->getString('_route');
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function getMenuItems(): array
    {
        return [
            $this->buildMenuItem(
                label: 'Accueil',
                routeName: 'app_home',
            ),
            $this->buildMenuItem(
                label: 'Sujets',
                routeName: 'app_topic_list',
                otherActiveRoute: [
                    'app_topic_show',
                    'app_topic_new',
                    'app_topic_delete',
                    'app_topic_edit',
                ]
            ),
        ];
    }

    /**
     * @param string $label
     * @param string $routeName
     * @param array<string> $otherActiveRoute
     * @return array<string,mixed>
     */
    private function buildMenuItem(string $label, string $routeName, array $otherActiveRoute = []): array
    {
        return [
            'label' => $label,
            'link' => $this->generator->generate($routeName),
            'isActive' => in_array($this->activeRoute, [...$otherActiveRoute, $routeName]),
        ];
    }
}
