<?php

namespace App\Twig\Components\Atoms\Table;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'TableCell', template: '@Atoms/Table/Cell.html.twig')]
class TableCell {}
