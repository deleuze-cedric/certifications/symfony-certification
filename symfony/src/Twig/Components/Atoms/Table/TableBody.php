<?php

namespace App\Twig\Components\Atoms\Table;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'TableBody', template: '@Atoms/Table/Body.html.twig')]
class TableBody {}
