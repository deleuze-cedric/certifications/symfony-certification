<?php

namespace App\Twig\Components\Atoms\Table;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'TableHeaderCell', template: '@Atoms/Table/HeaderCell.html.twig')]
class TableHeaderCell {}
