<?php

namespace App\Twig\Components\Atoms\Table;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'TableRow', template: '@Atoms/Table/Row.html.twig')]
class TableRow
{
    public bool $striped = false;

}
