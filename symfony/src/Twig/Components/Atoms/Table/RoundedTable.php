<?php

namespace App\Twig\Components\Atoms\Table;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'RoundedTable', template: '@Molecules/Table/RoundedTable.html.twig')]
class RoundedTable
{
    public bool $border = false;

}
