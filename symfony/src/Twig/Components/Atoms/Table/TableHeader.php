<?php

namespace App\Twig\Components\Atoms\Table;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'TableHeader', template: '@Atoms/Table/Header.html.twig')]
class TableHeader {}
