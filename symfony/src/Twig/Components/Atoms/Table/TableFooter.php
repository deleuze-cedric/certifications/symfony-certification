<?php

namespace App\Twig\Components\Atoms\Table;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'TableFooter', template: '@Atoms/Table/Footer.html.twig')]
class TableFooter {}
