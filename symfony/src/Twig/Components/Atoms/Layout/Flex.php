<?php

namespace App\Twig\Components\Atoms\Layout;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'Flex', template: '@Atoms/Layout/Flex.html.twig')]
class Flex
{
    public ?int $gap = null;
    public ?string $justify = null;
    public ?string $direction = null;
    public ?string $align = null;
    public bool $grow = false;
    public string $class = '';
    public bool $wrap = false;
}
