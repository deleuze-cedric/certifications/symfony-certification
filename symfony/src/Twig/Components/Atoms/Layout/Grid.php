<?php

namespace App\Twig\Components\Atoms\Layout;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent(name: 'Grid', template: '@Atoms/Layout/Grid.html.twig')]
class Grid
{
    public ?int $cols = null;

    public ?int $gap = null;

}
