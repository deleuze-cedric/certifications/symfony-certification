<?php

namespace App\EntityGenerator;

use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Throwable;

#[AutoconfigureTag(self::SHELL_GENERATOR_TAG)]
interface ShellEntityGeneratorInterface
{
    public const SHELL_GENERATOR_TAG = AbstractEntityGenerator::GENERATOR_TAG . '.shell';

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param QuestionHelper $helper
     * @throws Throwable
     */
    public function askParams(InputInterface $input, OutputInterface $output, QuestionHelper $helper): void;
}
