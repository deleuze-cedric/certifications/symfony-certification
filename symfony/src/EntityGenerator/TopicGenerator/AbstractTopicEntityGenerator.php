<?php

namespace App\EntityGenerator\TopicGenerator;

use App\Entity\Resource\UrlResource;
use App\Entity\Topic\Topic;
use App\EntityGenerator\AbstractEntityGenerator;
use App\Repository\Topic\TopicRepository;
use App\Service\Topic\CreateTopicBundleService;
use App\Service\Topic\SlugifyTopicNameService;
use App\Utils\AliceFixtureGenerator;
use App\Utils\CreateFileService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AutoconfigureTag(self::TOPIC_GENERATOR_TAG)]
abstract class AbstractTopicEntityGenerator extends AbstractEntityGenerator
{
    public const TOPIC_GENERATOR_TAG = self::GENERATOR_TAG . '.topic';

    /** @var NestedTreeRepository<Topic> $topicNestedTreeRepository */
    protected NestedTreeRepository $topicNestedTreeRepository;

    protected ?Topic $mainTopic;

    public function __construct(
        protected readonly TopicRepository          $topicRepository,
        protected readonly EntityManagerInterface   $entityManager,
        protected readonly LoggerInterface          $logger,
        protected readonly SlugifyTopicNameService  $slugifyTopicNameService,
        protected readonly HttpClientInterface      $client,
        protected readonly string                   $topicBundleDir,
        protected readonly string                   $gitignoreFilename,
        protected readonly CreateFileService        $createFileService,
        protected readonly CreateTopicBundleService $createTopicBundleService,
        protected readonly AliceFixtureGenerator    $aliceFixtureGenerator,
        protected readonly string                   $topicFixturesPath,
    ) {
        /** @var NestedTreeRepository<Topic> $nestedTreeRepository */
        $nestedTreeRepository = $this->entityManager->getRepository(Topic::class);
        $this->topicNestedTreeRepository = $nestedTreeRepository;
    }

    protected function createTopic(string $name, string $url, ?Topic $parentTopic, bool $createBundle): Topic
    {
        $existingTopic = $this->topicRepository->findOneBy(['name' => $name, 'parent' => $parentTopic]);
        if ($existingTopic) {
            $this->logger->info('Topic : "' . $name . '" already exists.');
            return $existingTopic;
        }

        $this->logger->info('Creating topic : "' . $name . '" (' . strlen($name) . ' chars).');
        $bundleName = $this->topicNameToBundleName($name);

        $urlResource = new UrlResource();
        $urlResource->setUrl($url);

        $topic = new Topic();
        $topic->setName($name)
            ->setParent($parentTopic)
            ->setBundleName(!$createBundle ? null : $bundleName)
            ->addResource($urlResource)
        ;

        return $topic;
    }

    protected function topicNameToBundleName(string $topic): string
    {
        return $this->slugifyTopicNameService->execute($topic) . 'TopicBundle';
    }

    /**
     * @throws Exception
     */
    protected function askMainTopicQuestion(InputInterface $input, OutputInterface $output, QuestionHelper $helper): void
    {
        $topics = $this->topicRepository->findAll();

        if (!$topics) {
            return;
        }

        $noneTopic = 'No parent topic';

        $topicsChoices = [
            ...$topics,
            $noneTopic,
        ];

        $topicQuestion = new ChoiceQuestion('Wath is the parent topic ? (none by default)', $topicsChoices, $noneTopic);
        $topicQuestion->setMultiline(true)
            ->setTrimmable(true)
        ;


        /** @var Topic|null $mainTopic */
        $mainTopic = $helper->ask($input, $output, $topicQuestion);
        $this->mainTopic = $mainTopic;
    }


    abstract public function generate(): void;
}
