<?php

namespace App\EntityGenerator\TopicGenerator;

use App\Entity\Resource\UrlResource;
use App\Entity\Topic\Topic;
use App\EntityGenerator\ShellEntityGeneratorInterface;
use DOMElement;
use DOMNode;
use Exception;
use stdClass;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SymfonyTopicEntityGenerator extends AbstractTopicEntityGenerator implements ShellEntityGeneratorInterface
{
    private const GITHUB_DOMAIN = 'https://github.com';
    private const REPO_URL = self::GITHUB_DOMAIN . '/ThomasBerends/symfony-certification-preparation-list';

    /** @var array<array|mixed> */
    private array $topicAsFixture = [];

    /** @var array<array|mixed> */
    private array $resourceAsFixture = [];

    /** @var array<string> */
    private array $topicAlreadyRegistered;

    private string $version;

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function generate(): void
    {
        $this->topicAlreadyRegistered = [];

        $url = self::REPO_URL . "/blob/$this->version/readme.md";

        $cleanVersion = str_replace('.0', '', $this->version);
        $symfonyDir = $this->topicFixturesPath . "/symfony_$cleanVersion";
        $generatedTopicFilename = 'generated_topic.yml';
        $generatedResourceFilename = 'generated_resource.yml';
        $topicYamlFile = $this->aliceFixtureGenerator->initYamlFile($symfonyDir . "/" . $generatedTopicFilename, ['topic.yml', $generatedResourceFilename]);
        $resourcesYamlFile = $this->aliceFixtureGenerator->initYamlFile($symfonyDir . "/" . $generatedResourceFilename, []);

        $readme = $this->getReadme($url);
        $this->readReadmeLink($readme, function (DOMElement $element) {
            $this->iterateDomElement($element, $this->mainTopic, true);
        });

        $this->aliceFixtureGenerator->writeEntities($topicYamlFile, Topic::class, $this->topicAsFixture);
        $this->aliceFixtureGenerator->writeEntities($resourcesYamlFile, UrlResource::class, $this->resourceAsFixture);
    }

    public function readReadmeLink(string $readme, callable $function): void
    {
        $readme = str_replace(['', "\n"], '', $readme);
        $crawler = new Crawler($readme);
        $crawler->filter('article > ul')
            ->each(function (Crawler $parentCrawler) use ($function) {
                foreach ($parentCrawler as $element) {
                    $function($element);
                }
            })
        ;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     */
    protected function iterateDomElement(
        DOMNode $element,
        ?Topic  $topic = null,
        bool    $isMainProcess = false
    ): void {
        /* @var DOMNode $subElement */
        foreach ($element->childNodes as $subElement) {
            switch ($subElement->nodeName) {
                case "ul":
                    $this->iterateDomElement($subElement, $topic, $isMainProcess);
                    break;
                case "li":
                    if ($subElement->hasChildNodes()) {
                        /* @var DOMNode $iteratedEl */
                        foreach ($subElement->childNodes as $iteratedEl) {
                            if ($iteratedEl->nodeName === "a") {
                                $value = $iteratedEl->nodeValue;
                                $href = $this->getHrefAttribute($iteratedEl);

                                if (is_null($value) || is_null($href)) {
                                    continue;
                                }

                                $valueMd5 = md5($value);
                                if ($href && !in_array($valueMd5, $this->topicAlreadyRegistered)) {
                                    $this->topicAlreadyRegistered[] = $valueMd5;
                                    if (!$isMainProcess) {
                                        if (!is_null($topic)) {
                                            $createdTopic = $this->createTopic($value, $href, $topic, false);
                                        }
                                    } else {
                                        $createdTopic = $this->createTopic($value, $href, $topic, true);
                                        $this->saveChapters($createdTopic, $href);
                                    }
                                    if (isset($createdTopic)) {
                                        $topicFixtureIdentifier = $createdTopic->getFixtureIdentifier(false);
                                        $this->topicAsFixture[$topicFixtureIdentifier] = $this->aliceFixtureGenerator->normalizeEntity($createdTopic);
                                        foreach ($createdTopic->getResources() as $resource) {
                                            $resourceFixtureIdentifier = $resource->getFixtureIdentifier(false);
                                            $this->resourceAsFixture[$resourceFixtureIdentifier] = $this->aliceFixtureGenerator->normalizeEntity($resource);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }

    protected function getHrefAttribute(
        DOMNode $element
    ): ?string {
        return $element->attributes?->getNamedItem('href')?->textContent;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    protected function getReadme(string $url): string
    {
        $response = $this->client->request('GET', $url);
        $status = $response->getStatusCode();

        if ($status !== Response::HTTP_OK) {
            throw new Exception("Impossible to retrieve page content (" . $status . "): " . $url);
        }

        $content = $response->getContent();

        if (!json_validate($content)) {
            throw new Exception('Invalid json content : ' . $content);
        }

        /** @var stdClass $decodedContent */
        $decodedContent = json_decode($content);

        $readme = $decodedContent->payload?->blob?->richText;

        if (!$readme) {
            throw new Exception('Empty readme : ' . $url);
        }

        return str_replace('href="/', 'href="' . self::GITHUB_DOMAIN . '/', $readme);
    }

    protected function saveChapters(Topic $topic, string $topicUrl): void
    {
        try {
            $readme = $this->getReadme($topicUrl);

            $this->readReadmeLink($readme, function (DOMElement $element) use ($topic) {
                $this->iterateDomElement($element, $topic);
            });
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface|Exception $e) {
            $this->logger->error("Impossible to create topic Bundle : " . $e->getMessage());
        }
    }

    public function askParams(InputInterface $input, OutputInterface $output, QuestionHelper $helper): void
    {
        $versionQuestion = new ChoiceQuestion('Which version would you import ?', [
            'main',
            '6.0',
            '5.0',
            '4.0',
            '3.0',
        ], 'main');

        $this->version = ($helper->ask($input, $output, $versionQuestion) ?? 'main') . '';

        $this->askMainTopicQuestion($input, $output, $helper);
    }
}
