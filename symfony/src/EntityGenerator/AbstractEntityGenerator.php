<?php

declare(strict_types=1);

namespace App\EntityGenerator;

use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Throwable;

#[AutoconfigureTag(self::GENERATOR_TAG)]
abstract class AbstractEntityGenerator
{
    public const GENERATOR_TAG = 'app.generator';

    /**
     * @throws Throwable
     */
    abstract public function generate(): void;
}
