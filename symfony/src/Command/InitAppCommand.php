<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

#[AsCommand(name: 'app:init')]
class InitAppCommand extends Command
{
    public function __construct(
        private readonly UserRepository              $userRepository,
        private readonly EntityManagerInterface      $entityManager,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly ValidatorInterface          $validator,
    ) {
        parent::__construct('Intialize app');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->userRepository->findBy(['roles' => User::ROLE_ADMIN])) {
            $this->createAdmin($input, $output);
            $this->entityManager->flush();
        }

        return Command::SUCCESS;
    }

    protected function createAdmin(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('<info>Admin creation</info>>');

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $emailQuestion = new Question('Email : ');
        $pseudoQuestion = new Question('Pseudo : ');
        $passwordQuestion = new Question('Password : ');

        $passwordQuestion->setHidden(true);

        do {

            /** @var string $email */
            $email = $helper->ask($input, $output, $emailQuestion);
            /** @var string $pseudo */
            $pseudo = $helper->ask($input, $output, $pseudoQuestion);
            /** @var string $password */
            $password = $helper->ask($input, $output, $passwordQuestion);

            $admin = new User();
            $hashedPassword = $this->passwordHasher->hashPassword($admin, $password);

            $admin->setRoles([User::ROLE_ADMIN])
                ->setEmail($email)
                ->setPassword($hashedPassword)
                ->setPseudo($pseudo)
            ;

            $errors = $this->validator->validate($admin);

            foreach ($errors as $error) {
                $output->writeln(
                    "<error>" .
                    "Invalid value for " . $error->getPropertyPath() . " : " . $error->getMessage() .
                    "</error>"
                );
            }

        } while ($errors->count() > 0);

        $this->entityManager->persist($admin);
    }
}
