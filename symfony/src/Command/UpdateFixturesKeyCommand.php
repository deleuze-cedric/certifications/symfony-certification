<?php

namespace App\Command;

use App\Interface\FixturableEntityInterface;
use App\Utils\AliceFixtureGenerator;
use DirectoryIterator;
use Nelmio\Alice\Loader\NativeLoader;
use Nelmio\Alice\Throwable\LoadingThrowable;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Yaml\Yaml;
use Throwable;

#[AsCommand(name: 'app:update-fixtures-key')]
final class UpdateFixturesKeyCommand extends Command
{
    private SymfonyStyle $style;
    private readonly NativeLoader $dataLoader;

    public function __construct(
        private readonly string                  $topicFixturesPath,
        protected readonly AliceFixtureGenerator $aliceFixtureGenerator,
    ) {
        $this->dataLoader = new NativeLoader();
        parent::__construct('Update topic fixtures');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->style = new SymfonyStyle($input, $output);

        $this->processDir($this->topicFixturesPath);

        return Command::SUCCESS;
    }


    /**
     * @throws LoadingThrowable
     * @throws ExceptionInterface
     */
    private function processDir(string $dir): void
    {
        $dirIterator = new DirectoryIterator($dir);

        /** @var DirectoryIterator $file */
        foreach ($dirIterator as $file) {
            if ($file->isDot()) {
                continue;
            }

            $path = $file->getPath() . '/' . $file->getFilename();

            if ($file->isDir()) {
                $this->processDir($path);
            } elseif ($file->isFile()) {
                $this->style->info('Processing : ' . $path);
                $entities = $this->dataLoader->loadFile($path)->getObjects();
                $newData = [];

                /** @var array<string, array<string, object>> $yamlContent */
                $yamlContent = Yaml::parseFile($path);

                foreach ($entities as $entityKey => $entity) {
                    $entityClass = $entity::class;

                    if (isset($yamlContent[$entityClass])) {

                        if (!isset($newData[$entityClass])) {
                            $newData[$entityClass] = [];
                        }

                        foreach ($yamlContent[$entityClass] as $yamlEntityKey => $yamlEntityData) {
                            if ($yamlEntityKey === $entityKey) {
                                if ($entity instanceof FixturableEntityInterface) {
                                    $yamlIdentifier = $entity->getFixtureIdentifier(false);
                                    $newData[$entityClass][$yamlIdentifier] = $this->aliceFixtureGenerator->normalizeEntity($entity);
                                } else {
                                    $newData[$entityClass][$entityKey] = $yamlContent[$entityClass][$entityKey];
                                }
                                break;
                            }
                        }
                    }
                }

                /** @var array<string> $include */
                $include = $yamlContent['include'] ?? [];

                $newFile = $this->aliceFixtureGenerator->initYamlFile($path, $include);
                $this->aliceFixtureGenerator->writeYaml($newFile, $newData);
                unset($newFile);
            }
        }
    }
}
