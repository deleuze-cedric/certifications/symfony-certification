<?php

namespace App\Command\Topic;

use App\EntityGenerator\ShellEntityGeneratorInterface;
use App\EntityGenerator\TopicGenerator\AbstractTopicEntityGenerator;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Throwable;

#[AsCommand(name: 'app:generate-topics')]
final class GenerateTopicsCommand extends Command
{
    /**
     * @param iterable<AbstractTopicEntityGenerator&ShellEntityGeneratorInterface> $generators
     */
    public function __construct(
        #[TaggedIterator(AbstractTopicEntityGenerator::TOPIC_GENERATOR_TAG)]
        private iterable $generators
    ) {
        $filteredGenerators = $this->generators;
        foreach ($this->generators as $index => $generator) {
            if (!$generator instanceof ShellEntityGeneratorInterface) {
                unset($filteredGenerators[$index]);
            }
        }
        $this->generators = $filteredGenerators;

        parent::__construct('Generate Topics');
    }

    protected function configure(): void
    {
        $this->addArgument('version', InputArgument::OPTIONAL, 'Symfony version (3.0, 4.0, ...)', 'main');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $generators = iterator_to_array($this->generators);

        switch (count($generators)) {
            case 0:
                throw new Exception('No generator found');
            case 1:
                $firstGeneratorKey = array_key_first($generators);
                $selectedGeneratorClass = $generators[$firstGeneratorKey]::class;
                break;
            default:
                $question = new ChoiceQuestion(
                    'Which generator would you execute ?',
                    array_map(function (ShellEntityGeneratorInterface $generator): string {
                        return $generator::class;
                    }, $generators),
                );
                $selectedGeneratorClass = $helper->ask($input, $output, $question);
                break;
        }

        $generator = null;

        foreach ($this->generators as $iteratedGenerator) {
            if ($selectedGeneratorClass === $iteratedGenerator::class) {
                $generator = $iteratedGenerator;
                break;
            }
        }

        if (!$generator) {
            throw new Exception('Generator not found');
        }

        $output->writeln('Generate topic with : ' . $selectedGeneratorClass);

        $generator->askParams($input, $output, $helper);
        $generator->generate();

        return Command::SUCCESS;
    }

}
