<?php

namespace App\Trait;

trait FixturableEntityTrait
{
    public function getFixtureIdentifier(bool $asResource): string
    {
        return ($asResource ? '@' : '') . md5($this->getYamlIdentifierFieldValue());
    }
}
