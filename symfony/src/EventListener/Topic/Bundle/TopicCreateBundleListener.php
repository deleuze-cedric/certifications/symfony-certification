<?php

namespace App\EventListener\Topic\Bundle;

use App\Entity\Topic\Topic;
use App\Service\Topic\CreateTopicBundleService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsEntityListener(event: Events::postPersist, entity: Topic::class)]
final readonly class TopicCreateBundleListener
{
    public function __construct(
        private CreateTopicBundleService $createTopicBundleService,
        private KernelInterface          $kernel
    ) {}

    public function postPersist(Topic $topic): void
    {
        if ("dev" !== $this->kernel->getEnvironment()) {
            return;
        }

        if (!$topic->getBundleName()) {
            return;
        }

        $this->createTopicBundleService->execute($topic);
    }
}
