<?php

namespace App\EventListener\Topic\Bundle;

use App\Entity\Topic\Topic;
use App\Service\Topic\CreateTopicBundleService;
use App\Service\Topic\MoveTopicBundleService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsEntityListener(event: Events::postUpdate, entity: Topic::class)]
final readonly class TopicMoveBundleListener
{
    public function __construct(
        private CreateTopicBundleService $createTopicBundleService,
        private MoveTopicBundleService   $moveTopicBundleService,
        private KernelInterface          $kernel
    ) {}

    public function postUpdate(Topic $topic, PreUpdateEventArgs $event): void
    {
        if ("dev" !== $this->kernel->getEnvironment()) {
            return;
        }

        $bundleNameField = 'bundleName';

        if (!$event->hasChangedField($bundleNameField)) {
            return;
        }

        /** @var string|null $oldBundleName */
        $oldBundleName = $event->getOldValue($bundleNameField);
        /** @var string|null $newBundleName */
        $newBundleName = $event->getNewValue($bundleNameField);

        if ($newBundleName) {
            if (!$oldBundleName) {
                $this->createTopicBundleService->execute($topic);
            } else {
                $this->moveTopicBundleService->execute($topic, $oldBundleName);
            }
        }
    }
}
