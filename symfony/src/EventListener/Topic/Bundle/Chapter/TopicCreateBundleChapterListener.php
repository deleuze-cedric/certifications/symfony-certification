<?php

namespace App\EventListener\Topic\Bundle\Chapter;

use App\Entity\Topic\Topic;
use App\Service\Topic\Bundle\Chapter\CreateTopicBundleChapterService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsEntityListener(event: Events::postPersist, entity: Topic::class)]
final readonly class TopicCreateBundleChapterListener
{
    public function __construct(
        private CreateTopicBundleChapterService $createTopicBundleChapterService,
        private KernelInterface                 $kernel
    ) {}

    public function postPersist(Topic $topic): void
    {
        if ("dev" !== $this->kernel->getEnvironment()) {
            return;
        }

        if (!$topic->isBundleChapter()) {
            return;
        }

        $this->createTopicBundleChapterService->execute($topic);
    }
}
