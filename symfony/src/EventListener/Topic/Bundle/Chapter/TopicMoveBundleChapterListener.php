<?php

namespace App\EventListener\Topic\Bundle\Chapter;

use App\Entity\Topic\Topic;
use App\Service\Topic\Bundle\Chapter\CreateTopicBundleChapterService;
use App\Service\Topic\Bundle\Chapter\MoveTopicBundleChapterService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsEntityListener(event: Events::postUpdate, entity: Topic::class)]
final readonly class TopicMoveBundleChapterListener
{
    public function __construct(
        private CreateTopicBundleChapterService $createTopicBundleChapterService,
        private MoveTopicBundleChapterService   $moveTopicBundleChapterService,
        private KernelInterface                 $kernel
    ) {}

    public function postUpdate(Topic $topic, PreUpdateEventArgs $event): void
    {
        if ("dev" !== $this->kernel->getEnvironment()) {
            return;
        }

        if (!$topic->isBundleChapter()) {
            return;
        }

        $nameField = 'name';
        $parentField = 'parent';

        if (!$event->hasChangedField($nameField) && !$event->hasChangedField($parentField)) {
            return;
        }

        $oldName = $event->getOldValue($nameField);
        /** @var Topic|null $oldParent */
        $oldParent = $event->getOldValue($nameField);
        /** @var Topic|null $newParent */
        $newParent = $event->getNewValue($nameField);

        $oldBundleName = $oldParent?->getBundleName();
        $newBundleName = $newParent?->getBundleName();

        if ($newBundleName) {
            if (!$oldBundleName) {
                $this->createTopicBundleChapterService->execute($topic);
            } elseif (is_string($oldName)) {
                $this->moveTopicBundleChapterService->execute($topic, $oldBundleName, $oldName);
            }
        }
    }
}
