<?php

namespace App\Interface;

interface FixturableEntityInterface
{
    public function getFixtureIdentifier(bool $asResource): string;

    public function getYamlIdentifierFieldValue(): string;
}
