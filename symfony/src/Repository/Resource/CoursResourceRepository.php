<?php

namespace App\Repository\Resource;

use App\Entity\Resource\CoursResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CoursResource>
 *
 * @method CoursResource|null find($id, $lockMode = null, $lockVersion = null)
 * @method CoursResource|null findOneBy(array $criteria, array $orderBy = null)
 * @method CoursResource[]    findAll()
 * @method CoursResource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursResourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CoursResource::class);
    }
}
