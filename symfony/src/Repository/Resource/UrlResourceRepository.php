<?php

namespace App\Repository\Resource;

use App\Entity\Resource\UrlResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UrlResource>
 *
 * @method UrlResource|null find($id, $lockMode = null, $lockVersion = null)
 * @method UrlResource|null findOneBy(array $criteria, array $orderBy = null)
 * @method UrlResource[]    findAll()
 * @method UrlResource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UrlResourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UrlResource::class);
    }
}
