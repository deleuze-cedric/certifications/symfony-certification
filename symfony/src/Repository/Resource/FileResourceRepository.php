<?php

namespace App\Repository\Resource;

use App\Entity\Resource\FileResource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FileResource>
 *
 * @method FileResource|null find($id, $lockMode = null, $lockVersion = null)
 * @method FileResource|null findOneBy(array $criteria, array $orderBy = null)
 * @method FileResource[]    findAll()
 * @method FileResource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileResourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FileResource::class);
    }
}
