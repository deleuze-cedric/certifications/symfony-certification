<?php

namespace App\Repository\Test;

use App\Entity\Test\LeitnerTest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<LeitnerTest>
 *
 * @method LeitnerTest|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeitnerTest|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeitnerTest[]    findAll()
 * @method LeitnerTest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeitnerTestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeitnerTest::class);
    }
}
