<?php

namespace App\Repository\Test;

use App\Entity\Test\TestLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<TestLine>
 *
 * @method TestLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestLine[]    findAll()
 * @method TestLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestLine::class);
    }
}
