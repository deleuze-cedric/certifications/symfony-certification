<?php

namespace App\Entity\Resource;

use App\Entity\Test\Question;
use App\Entity\Topic\Topic;
use App\Interface\FixturableEntityInterface;
use App\Trait\FixturableEntityTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Loggable\Loggable;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap(['cours' => CoursResource::class, 'url' => UrlResource::class, 'file' => FileResource::class])]
#[ORM\Entity]
#[Gedmo\Loggable]
abstract class Resource implements Loggable, FixturableEntityInterface
{
    use FixturableEntityTrait;

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;

    /** @var Collection<int, Topic> */
    #[ORM\ManyToMany(targetEntity: Topic::class, mappedBy: 'resources')]
    private Collection $topics;

    /** @var Collection<int, Question> */
    #[ORM\ManyToMany(targetEntity: Question::class, mappedBy: 'resources')]
    private Collection $questions;

    public function __construct()
    {
        $this->topics = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Topic>
     */
    public function getTopics(): Collection
    {
        return $this->topics;
    }

    public function addTopic(Topic $topic): static
    {
        if (!$this->topics->contains($topic)) {
            $this->topics->add($topic);
            $topic->addResource($this);
        }

        return $this;
    }

    public function removeTopic(Topic $topic): static
    {
        if ($this->topics->removeElement($topic)) {
            $topic->removeResource($this);
        }

        return $this;
    }

    abstract public function __toString(): string;

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): static
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->addResource($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): static
    {
        if ($this->questions->removeElement($question)) {
            $question->removeResource($this);
        }

        return $this;
    }
}
