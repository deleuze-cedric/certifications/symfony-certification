<?php

namespace App\Entity\Resource;

use App\Entity\Media;
use App\Repository\Resource\FileResourceRepository;
use App\Utils\AliceFixtureGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: FileResourceRepository::class)]
class FileResource extends Resource
{
    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private Media $media;

    public function getMedia(): Media
    {
        return $this->media;
    }

    public function setMedia(Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getFilename();
    }

    #[Groups([AliceFixtureGenerator::GROUP])]
    #[SerializedName('name')]
    public function getFilename(): string
    {
        return $this->getMedia()->getName();
    }

    public function getYamlIdentifierFieldValue(): string
    {
        return $this->getFilename();
    }
}
