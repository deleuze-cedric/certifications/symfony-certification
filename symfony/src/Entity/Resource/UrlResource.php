<?php

namespace App\Entity\Resource;

use App\Repository\Resource\UrlResourceRepository;
use App\Utils\AliceFixtureGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UrlResourceRepository::class)]
class UrlResource extends Resource
{
    #[ORM\Column(length: 255)]
    #[Groups(AliceFixtureGenerator::GROUP)]
    private string $url;

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function __toString(): string
    {
        return $this->url;
    }

    public function getYamlIdentifierFieldValue(): string
    {
        return $this->getUrl();
    }
}
