<?php

namespace App\Entity\Test;

use App\Repository\Test\LeitnerTestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LeitnerTestRepository::class)]
class LeitnerTest extends Test
{
    public function addTestLine(TestLine $testLine): static
    {
        $this->getTestLines()->clear();

        return parent::addTestLine($testLine);
    }
}
