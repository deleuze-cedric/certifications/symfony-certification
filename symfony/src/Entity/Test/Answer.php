<?php

namespace App\Entity\Test;

use App\Interface\FixturableEntityInterface;
use App\Repository\Test\AnswerRepository;
use App\Trait\FixturableEntityTrait;
use App\Utils\AliceFixtureGenerator;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Uid\Uuid;
use Gedmo\Mapping\Annotation as Gedmo;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[Gedmo\Loggable]
#[ORM\Entity(repositoryClass: AnswerRepository::class)]
class Answer implements FixturableEntityInterface
{
    use FixturableEntityTrait;
    use SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;

    #[ORM\ManyToOne(inversedBy: 'answers')]
    #[ORM\JoinColumn(nullable: false)]
    private Question $question;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(AliceFixtureGenerator::GROUP)]
    private string $value;

    #[ORM\Column]
    #[Groups(AliceFixtureGenerator::GROUP)]
    private bool $isRight = false;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getQuestion(): Question
    {
        return $this->question;
    }

    public function setQuestion(Question $question): static
    {
        $this->question = $question;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;

        return $this;
    }

    public function isIsRight(): bool
    {
        return $this->isRight;
    }

    public function setIsRight(bool $isRight): static
    {
        $this->isRight = $isRight;

        return $this;
    }

    public function getYamlIdentifierFieldValue(): string
    {
        return $this->getValue();
    }

    #[Groups(AliceFixtureGenerator::GROUP)]
    #[SerializedName('question')]
    public function getQuestionAsYamlIdentifier(): string
    {
        return $this->question->getFixtureIdentifier(true);
    }
}
