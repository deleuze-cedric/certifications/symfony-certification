<?php

namespace App\Entity\Test;

use App\Entity\User;
use App\Repository\Test\TestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap(['leitner' => LeitnerTest::class, 'quizz' => Quizz::class])]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[ORM\Entity(repositoryClass: TestRepository::class)]
abstract class Test
{
    use SoftDeleteableEntity;
    use TimestampableEntity;
    use BlameableEntity;

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;
    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $programmedFor = null;

    #[ORM\Column]
    private ?bool $isDone = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    /** @var Collection<int, TestLine>  */
    #[ORM\OneToMany(mappedBy: 'test', targetEntity: TestLine::class, orphanRemoval: true)]
    private Collection $testLines;

    public function __construct()
    {
        $this->testLines = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getProgrammedFor(): ?\DateTimeImmutable
    {
        return $this->programmedFor;
    }

    public function setProgrammedFor(?\DateTimeImmutable $programmedFor): static
    {
        $this->programmedFor = $programmedFor;

        return $this;
    }

    public function isIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(bool $isDone): static
    {
        $this->isDone = $isDone;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, TestLine>
     */
    public function getTestLines(): Collection
    {
        return $this->testLines;
    }

    public function addTestLine(TestLine $testLine): static
    {
        if (!$this->testLines->contains($testLine)) {
            $this->testLines->add($testLine);
            $testLine->setTest($this);
        }

        return $this;
    }

    public function removeTestLine(TestLine $testLine): static
    {
        if ($this->testLines->removeElement($testLine)) {
            // set the owning side to null (unless already changed)
            if ($testLine->getTest() === $this) {
                $testLine->setTest(null);
            }
        }

        return $this;
    }
}
