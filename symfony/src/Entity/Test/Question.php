<?php

namespace App\Entity\Test;

use App\Entity\Resource\Resource;
use App\Entity\Topic\Topic;
use App\Interface\FixturableEntityInterface;
use App\Repository\Test\QuestionRepository;
use App\Trait\FixturableEntityTrait;
use App\Utils\AliceFixtureGenerator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Uid\Uuid;

#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', hardDelete: true)]
#[Gedmo\Loggable]
#[ORM\Entity(repositoryClass: QuestionRepository::class)]
class Question implements FixturableEntityInterface
{
    use FixturableEntityTrait;
    use SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;

    #[ORM\ManyToOne(inversedBy: 'questions')]
    #[ORM\JoinColumn(nullable: false)]
    private Topic $topic;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(AliceFixtureGenerator::GROUP)]
    private string $title;

    /** @var Collection<int, Answer> */
    #[ORM\OneToMany(mappedBy: 'question', targetEntity: Answer::class, orphanRemoval: true)]
    private Collection $answers;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(AliceFixtureGenerator::GROUP)]
    private ?string $explanation = null;

    /** @var Collection<int, Resource> */
    #[ORM\ManyToMany(targetEntity: Resource::class, inversedBy: 'questions')]
    private Collection $resources;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
        $this->resources = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getTopic(): Topic
    {
        return $this->topic;
    }

    public function setTopic(Topic $topic): static
    {
        $this->topic = $topic;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, Answer>
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(Answer $answer): static
    {
        if (!$this->answers->contains($answer)) {
            $this->answers->add($answer);
            $answer->setQuestion($this);
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getTopic()->getName() . ' => ' . $this->getTitle();
    }

    public function getExplanation(): ?string
    {
        return $this->explanation;
    }

    public function setExplanation(?string $explanation): static
    {
        $this->explanation = $explanation;

        return $this;
    }

    /**
     * @return Collection<int, Resource>
     */
    public function getResources(): Collection
    {
        return $this->resources;
    }

    public function addResource(Resource $resource): static
    {
        if (!$this->resources->contains($resource)) {
            $this->resources->add($resource);
        }

        return $this;
    }

    public function removeResource(Resource $resource): static
    {
        $this->resources->removeElement($resource);

        return $this;
    }

    /**
     * @return array<string>
     */
    #[Groups(AliceFixtureGenerator::GROUP)]
    #[SerializedName('resources')]
    public function getResourcesAsYamlIdentifier(): array
    {
        return $this->resources->map(function (Resource $resource): string {
            return $resource->getFixtureIdentifier(true);
        })->toArray();
    }

    public function getYamlIdentifierFieldValue(): string
    {
        return $this->getTitle();
    }
}
