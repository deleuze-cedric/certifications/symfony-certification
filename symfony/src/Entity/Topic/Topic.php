<?php

namespace App\Entity\Topic;

use App\Entity\Media;
use App\Entity\Resource\Resource;
use App\Entity\Test\Question;
use App\Interface\FixturableEntityInterface;
use App\Trait\FixturableEntityTrait;
use App\Utils\AliceFixtureGenerator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Gedmo\Tree\Traits\NestedSetEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[Gedmo\Tree(type: 'nested')]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt')]
#[ORM\Table(name: 'topic')]
#[UniqueEntity(fields: ['name'], message: 'Le nom est déjà utilisé')]
#[ORM\UniqueConstraint(fields: ['name', 'parent'])]
#[ORM\UniqueConstraint(fields: ['bundleName', 'parent'])]
#[ORM\UniqueConstraint(fields: ['slug'])]
#[ORM\Entity(repositoryClass: NestedTreeRepository::class)]
class Topic implements FixturableEntityInterface
{
    use BlameableEntity;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use NestedSetEntity;
    use FixturableEntityTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    #[Groups(AliceFixtureGenerator::GROUP)]
    private string $name;

    #[ORM\Column(length: 200, unique: true)]
    #[Gedmo\Slug(fields: ['name'])]
    private ?string $slug = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(AliceFixtureGenerator::GROUP)]
    private ?string $bundleName = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(AliceFixtureGenerator::GROUP)]
    private ?string $description = null;

    #[Gedmo\TreeParent]
    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    private ?self $parent = null;

    /** @var Collection<int, Topic> */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    #[ORM\OrderBy(['left' => 'ASC'])]
    private Collection $children;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?Media $image = null;

    /** @var Collection<int, Question> */
    #[ORM\OneToMany(mappedBy: 'topic', targetEntity: Question::class, orphanRemoval: true)]
    private Collection $questions;

    /** @var Collection<int, Resource> */
    #[ORM\ManyToMany(targetEntity: Resource::class, inversedBy: 'topics')]
    private Collection $resources;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->resources = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function getBundleName(): ?string
    {
        return $this->bundleName;
    }

    public function setBundleName(?string $bundleName): static
    {
        $this->bundleName = $bundleName;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?Media
    {
        return $this->image;
    }

    public function setImage(?Media $image): static
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection<int, Question>
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): static
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
            $question->setTopic($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): static
    {
        $this->questions->removeElement($question);
        return $this;
    }

    /**
     * @return Collection<int, Topic>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function __toString(): string
    {
        return implode('/', $this->getTopicPath());
    }

    /**
     * @return array<Topic>
     */
    public function getTopicPath(): array
    {
        return [
            ...$this->getParent()?->getTopicPath() ?? [],
            $this,
        ];
    }

    #[Groups(AliceFixtureGenerator::GROUP)]
    #[SerializedName('parent')]
    public function getParentIdentifierForYaml(): ?string
    {
        return $this->parent?->getFixtureIdentifier(true);
    }

    public function isBundleChapter(): bool
    {
        if (!$this->getParent()?->getBundleName()) {
            return false;
        }

        if ($this->getBundleName()) {
            return false;
        }

        return true;
    }

    /**
     * @param array<Question> $questions
     * @return void
     */
    public function setQuestions(array $questions): void
    {
        $this->questions = new ArrayCollection($questions);
    }

    /**
     * @return array<string>
     */
    #[Groups(AliceFixtureGenerator::GROUP)]
    #[SerializedName('resources')]
    public function getResourcesAsYamlIdentifier(): array
    {
        return $this->resources->map(function (Resource $resource): string {
            return $resource->getFixtureIdentifier(true);
        })->toArray();
    }

    /**
     * @return array<string>
     */
    #[Groups(AliceFixtureGenerator::GROUP)]
    #[SerializedName('questions')]
    public function getQuestionsAsYamlIdentifier(): array
    {
        return $this->questions->map(function (Question $question): string {
            return $question->getFixtureIdentifier(true);
        })->toArray();
    }

    /**
     * @return Collection<int, Resource>
     */
    public function getResources(): Collection
    {
        return $this->resources;
    }

    public function addResource(Resource $resource): static
    {
        if (!$this->resources->contains($resource)) {
            $this->resources->add($resource);
        }

        return $this;
    }

    public function removeResource(Resource $resource): static
    {
        $this->resources->removeElement($resource);

        return $this;
    }

    /**
     * @param array<int, Resource> $resources
     * @return void
     */
    public function setResources(array $resources): void
    {
        $this->resources = new ArrayCollection($resources);
    }

    public function getYamlIdentifierFieldValue(): string
    {
        return implode('/', $this->getTopicPath());
    }
}
