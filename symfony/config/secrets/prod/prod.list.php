<?php

return [
    'ADMIN_EMAIL' => null,
    'ADMIN_PASSWORD' => null,
    'ADMIN_PSEUDO' => null,
    'APP_SECRET' => null,
    'DATABASE_TYPE' => null,
    'DATABASE_URL' => null,
];
