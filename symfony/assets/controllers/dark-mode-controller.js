import {Controller} from "@hotwired/stimulus"

class DarkModeController extends Controller {

    static getDarkModeIcon() {
        return document.getElementById('theme-toggle-dark-icon');
    }

    static getLightModeIcon() {
        return document.getElementById('theme-toggle-light-icon');
    }

    connect() {
        this.element.addEventListener('click', this.toggleDarkMode);

        if (localStorage.getItem('theme') === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
            DarkModeController.getLightModeIcon().classList.remove('hidden');
        } else {
            DarkModeController.getDarkModeIcon().classList.remove('hidden');
        }
    }

    toggleDarkMode() {
        DarkModeController.getDarkModeIcon().classList.toggle('hidden');
        DarkModeController.getLightModeIcon().classList.toggle('hidden');

        const themeIndex = 'theme';
        const dark = 'dark';
        const light = 'light';
        const theme = localStorage.getItem(themeIndex);

        let setDarkMode = !document.documentElement.classList.contains(dark);

        if (theme) {
            setDarkMode = theme === light;
        }

        if (setDarkMode) {
            document.documentElement.classList.add(dark);
            localStorage.setItem(themeIndex, dark);
        } else {
            document.documentElement.classList.remove(dark);
            localStorage.setItem(themeIndex, light);
        }
    }

}

export default DarkModeController;