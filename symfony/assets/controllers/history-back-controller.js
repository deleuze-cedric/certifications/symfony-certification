import {Controller} from "@hotwired/stimulus"

class HistoryBackController extends Controller {

    connect() {
        this.element.addEventListener('click', this.historyBack);
    }

    historyBack() {
        history.back();
    }

}

export default HistoryBackController;