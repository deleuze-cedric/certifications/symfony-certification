import {Controller} from "@hotwired/stimulus"

class TabController extends Controller {

    static TAB_ITEM_CLASS = 'tab-item';
    static TAB_ITEM_DATA_TARGET_KEY = 'tabsTarget';
    static TAB_CLASS = 'tab-component';

    connect() {
        const items = this.element.getElementsByClassName(TabController.TAB_ITEM_CLASS);
        for (const item of items) {
            item.addEventListener('click', this.switchSelectedTab);
            if (item.ariaSelected === '1') {
                TabController.selectItem(item, this.element);
            }
        }
    }

    static selectContent(tabComponent, item) {
        const content = TabController.getContentFromItem(tabComponent, item);
        content.classList.remove('hidden');
    }

    static unselectContent(tabComponent, item) {
        const content = TabController.getContentFromItem(tabComponent, item);
        content.classList.add('hidden');
    }

    static getContentFromItem(tabComponent, item) {
        return tabComponent.querySelector(item.dataset[TabController.TAB_ITEM_DATA_TARGET_KEY]);
    }

    static selectItem(item, tabComponent) {
        item.ariaSelected = '1';
        item.classList.add('selected');
        TabController.selectContent(tabComponent, item);
    }

    static unselectItem(item, tabComponent) {
        item.ariaSelected = '0';
        item.classList.remove('selected');
        TabController.unselectContent(tabComponent, item);
    }

    switchSelectedTab() {
        let tabEl = this.closest('.' + TabController.TAB_CLASS);

        const items = tabEl.getElementsByClassName(TabController.TAB_ITEM_CLASS);
        for (const item of items) {
            if (item === this) {
                TabController.selectItem(item, tabEl);
            } else {
                TabController.unselectItem(item, tabEl);
            }
        }
    }

}

export default TabController;