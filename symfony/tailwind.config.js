/** @type {import('tailwindcss').Config} */
module.exports = {
    darkMode: 'class',
    content: [
        "./assets/**/*.css",
        "./assets/**/*.js",
        "./templates/**/*.html.twig",
        "./vendor/tales-from-a-dev/flowbite-bundle/templates/**/*.html.twig",
    ],
    theme: {
        extend: {},
    },
    plugins: ['flowbite'],
}

