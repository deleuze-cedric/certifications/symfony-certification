<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231123100341 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE abstract_log_entry_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE test_line_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE topic_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE answer (id UUID NOT NULL, question_id UUID NOT NULL, value VARCHAR(100) NOT NULL, description TEXT DEFAULT NULL, is_right BOOLEAN NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DADD4A251E27F6BF ON answer (question_id)');
        $this->addSql('COMMENT ON COLUMN answer.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN answer.question_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE cours_resource (id UUID NOT NULL, content TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN cours_resource.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE ext_log_entries (id INT NOT NULL, action VARCHAR(8) NOT NULL, logged_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(191) NOT NULL, version INT NOT NULL, data TEXT DEFAULT NULL, username VARCHAR(191) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX log_class_lookup_idx ON ext_log_entries (object_class)');
        $this->addSql('CREATE INDEX log_date_lookup_idx ON ext_log_entries (logged_at)');
        $this->addSql('CREATE INDEX log_user_lookup_idx ON ext_log_entries (username)');
        $this->addSql('CREATE INDEX log_version_lookup_idx ON ext_log_entries (object_id, object_class, version)');
        $this->addSql('COMMENT ON COLUMN ext_log_entries.data IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE file_resource (id UUID NOT NULL, media_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DC17B8BEA9FDD75 ON file_resource (media_id)');
        $this->addSql('COMMENT ON COLUMN file_resource.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN file_resource.media_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE leitner_test (id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN leitner_test.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE media (id UUID NOT NULL, original_name VARCHAR(100) NOT NULL, name VARCHAR(50) NOT NULL, path VARCHAR(255) NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN media.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE question (id UUID NOT NULL, topic_id INT NOT NULL, resource_answer_id UUID NOT NULL, title VARCHAR(100) NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6F7494E1F55203D ON question (topic_id)');
        $this->addSql('CREATE INDEX IDX_B6F7494E2FA1B98 ON question (resource_answer_id)');
        $this->addSql('COMMENT ON COLUMN question.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN question.resource_answer_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE quizz (id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN quizz.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE resource (id UUID NOT NULL, topic_id INT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_BC91F4161F55203D ON resource (topic_id)');
        $this->addSql('COMMENT ON COLUMN resource.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE test (id UUID NOT NULL, user_id UUID NOT NULL, programmed_for TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_done BOOLEAN NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D87F7E0CA76ED395 ON test (user_id)');
        $this->addSql('COMMENT ON COLUMN test.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN test.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN test.programmed_for IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE test_line (id INT NOT NULL, question_id UUID NOT NULL, test_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D46D9E391E27F6BF ON test_line (question_id)');
        $this->addSql('CREATE INDEX IDX_D46D9E391E5D0459 ON test_line (test_id)');
        $this->addSql('COMMENT ON COLUMN test_line.question_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN test_line.test_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE test_line_answer (test_line_id INT NOT NULL, answer_id UUID NOT NULL, PRIMARY KEY(test_line_id, answer_id))');
        $this->addSql('CREATE INDEX IDX_F1EDA726913AA18A ON test_line_answer (test_line_id)');
        $this->addSql('CREATE INDEX IDX_F1EDA726AA334807 ON test_line_answer (answer_id)');
        $this->addSql('COMMENT ON COLUMN test_line_answer.answer_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE topic (id INT NOT NULL, parent_id INT DEFAULT NULL, image_id UUID DEFAULT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(200) NOT NULL, bundle_name VARCHAR(255) DEFAULT NULL, description TEXT DEFAULT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, root INT DEFAULT NULL, lvl INT NOT NULL, lft INT NOT NULL, rgt INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9D40DE1B727ACA70 ON topic (parent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D40DE1B3DA5256D ON topic (image_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D40DE1B5E237E06727ACA70 ON topic (name, parent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D40DE1B625465C6727ACA70 ON topic (bundle_name, parent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D40DE1B989D9B62 ON topic (slug)');
        $this->addSql('COMMENT ON COLUMN topic.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE url_resource (id UUID NOT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN url_resource.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "user" (id UUID NOT NULL, avatar_id UUID DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, pseudo VARCHAR(20) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64986383B10 ON "user" (avatar_id)');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "user".avatar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A251E27F6BF FOREIGN KEY (question_id) REFERENCES question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cours_resource ADD CONSTRAINT FK_7B055ACBBF396750 FOREIGN KEY (id) REFERENCES resource (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE file_resource ADD CONSTRAINT FK_DC17B8BEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE file_resource ADD CONSTRAINT FK_DC17B8BBF396750 FOREIGN KEY (id) REFERENCES resource (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE leitner_test ADD CONSTRAINT FK_EECEE4F4BF396750 FOREIGN KEY (id) REFERENCES test (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E1F55203D FOREIGN KEY (topic_id) REFERENCES topic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E2FA1B98 FOREIGN KEY (resource_answer_id) REFERENCES resource (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE quizz ADD CONSTRAINT FK_7C77973DBF396750 FOREIGN KEY (id) REFERENCES test (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F4161F55203D FOREIGN KEY (topic_id) REFERENCES topic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE test ADD CONSTRAINT FK_D87F7E0CA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE test_line ADD CONSTRAINT FK_D46D9E391E27F6BF FOREIGN KEY (question_id) REFERENCES question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE test_line ADD CONSTRAINT FK_D46D9E391E5D0459 FOREIGN KEY (test_id) REFERENCES test (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE test_line_answer ADD CONSTRAINT FK_F1EDA726913AA18A FOREIGN KEY (test_line_id) REFERENCES test_line (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE test_line_answer ADD CONSTRAINT FK_F1EDA726AA334807 FOREIGN KEY (answer_id) REFERENCES answer (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1B727ACA70 FOREIGN KEY (parent_id) REFERENCES topic (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1B3DA5256D FOREIGN KEY (image_id) REFERENCES media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE url_resource ADD CONSTRAINT FK_DEF19A9FBF396750 FOREIGN KEY (id) REFERENCES resource (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D64986383B10 FOREIGN KEY (avatar_id) REFERENCES media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE bingo.bingo_config');
        $this->addSql('DROP TABLE bingo.bingo_tau_config');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SCHEMA bingo');
        $this->addSql('DROP SEQUENCE abstract_log_entry_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE test_line_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE topic_id_seq CASCADE');
        $this->addSql('CREATE TABLE bingo.bingo_config (cname VARCHAR(255) DEFAULT NULL, cvalue VARCHAR(255) DEFAULT NULL)');
        $this->addSql('CREATE TABLE bingo.bingo_tau_config (rule_idx INT DEFAULT NULL, tau_beg TEXT DEFAULT NULL, tau_end TEXT DEFAULT NULL)');
        $this->addSql('ALTER TABLE answer DROP CONSTRAINT FK_DADD4A251E27F6BF');
        $this->addSql('ALTER TABLE cours_resource DROP CONSTRAINT FK_7B055ACBBF396750');
        $this->addSql('ALTER TABLE file_resource DROP CONSTRAINT FK_DC17B8BEA9FDD75');
        $this->addSql('ALTER TABLE file_resource DROP CONSTRAINT FK_DC17B8BBF396750');
        $this->addSql('ALTER TABLE leitner_test DROP CONSTRAINT FK_EECEE4F4BF396750');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494E1F55203D');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494E2FA1B98');
        $this->addSql('ALTER TABLE quizz DROP CONSTRAINT FK_7C77973DBF396750');
        $this->addSql('ALTER TABLE resource DROP CONSTRAINT FK_BC91F4161F55203D');
        $this->addSql('ALTER TABLE test DROP CONSTRAINT FK_D87F7E0CA76ED395');
        $this->addSql('ALTER TABLE test_line DROP CONSTRAINT FK_D46D9E391E27F6BF');
        $this->addSql('ALTER TABLE test_line DROP CONSTRAINT FK_D46D9E391E5D0459');
        $this->addSql('ALTER TABLE test_line_answer DROP CONSTRAINT FK_F1EDA726913AA18A');
        $this->addSql('ALTER TABLE test_line_answer DROP CONSTRAINT FK_F1EDA726AA334807');
        $this->addSql('ALTER TABLE topic DROP CONSTRAINT FK_9D40DE1B727ACA70');
        $this->addSql('ALTER TABLE topic DROP CONSTRAINT FK_9D40DE1B3DA5256D');
        $this->addSql('ALTER TABLE url_resource DROP CONSTRAINT FK_DEF19A9FBF396750');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D64986383B10');
        $this->addSql('DROP TABLE answer');
        $this->addSql('DROP TABLE cours_resource');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE file_resource');
        $this->addSql('DROP TABLE leitner_test');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE quizz');
        $this->addSql('DROP TABLE resource');
        $this->addSql('DROP TABLE test');
        $this->addSql('DROP TABLE test_line');
        $this->addSql('DROP TABLE test_line_answer');
        $this->addSql('DROP TABLE topic');
        $this->addSql('DROP TABLE url_resource');
        $this->addSql('DROP TABLE "user"');
    }
}
