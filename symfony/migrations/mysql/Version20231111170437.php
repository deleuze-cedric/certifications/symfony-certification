<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231111170437 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE answer (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', question_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', value VARCHAR(100) NOT NULL, description LONGTEXT DEFAULT NULL, is_right TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_DADD4A251E27F6BF (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cours_resource (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(191) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(191) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE file_resource (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', media_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', UNIQUE INDEX UNIQ_DC17B8BEA9FDD75 (media_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE leitner_test (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', original_name VARCHAR(100) NOT NULL, name VARCHAR(50) NOT NULL, path VARCHAR(255) NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', topic_id INT NOT NULL, resource_answer_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(100) NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_B6F7494E1F55203D (topic_id), INDEX IDX_B6F7494E2FA1B98 (resource_answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quizz (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE resource (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', topic_id INT NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_BC91F4161F55203D (topic_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', programmed_for DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_done TINYINT(1) NOT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_D87F7E0CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_line (id INT AUTO_INCREMENT NOT NULL, question_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', test_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_D46D9E391E27F6BF (question_id), INDEX IDX_D46D9E391E5D0459 (test_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_line_answer (test_line_id INT NOT NULL, answer_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_F1EDA726913AA18A (test_line_id), INDEX IDX_F1EDA726AA334807 (answer_id), PRIMARY KEY(test_line_id, answer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE topic (id INT AUTO_INCREMENT NOT NULL, tree_root INT DEFAULT NULL, parent_id INT DEFAULT NULL, image_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(100) NOT NULL, slug VARCHAR(200) NOT NULL, bundle_name VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, lft INT NOT NULL, lvl INT NOT NULL, rgt INT NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_9D40DE1B989D9B62 (slug), INDEX IDX_9D40DE1BA977936C (tree_root), INDEX IDX_9D40DE1B727ACA70 (parent_id), UNIQUE INDEX UNIQ_9D40DE1B3DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE url_resource (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', url VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', avatar_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', email VARCHAR(180) NOT NULL, roles JSON NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, pseudo VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D64986383B10 (avatar_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A251E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
        $this->addSql('ALTER TABLE cours_resource ADD CONSTRAINT FK_7B055ACBBF396750 FOREIGN KEY (id) REFERENCES resource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE file_resource ADD CONSTRAINT FK_DC17B8BEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE file_resource ADD CONSTRAINT FK_DC17B8BBF396750 FOREIGN KEY (id) REFERENCES resource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE leitner_test ADD CONSTRAINT FK_EECEE4F4BF396750 FOREIGN KEY (id) REFERENCES test (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E1F55203D FOREIGN KEY (topic_id) REFERENCES topic (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E2FA1B98 FOREIGN KEY (resource_answer_id) REFERENCES resource (id)');
        $this->addSql('ALTER TABLE quizz ADD CONSTRAINT FK_7C77973DBF396750 FOREIGN KEY (id) REFERENCES test (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F4161F55203D FOREIGN KEY (topic_id) REFERENCES topic (id)');
        $this->addSql('ALTER TABLE test ADD CONSTRAINT FK_D87F7E0CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE test_line ADD CONSTRAINT FK_D46D9E391E27F6BF FOREIGN KEY (question_id) REFERENCES question (id)');
        $this->addSql('ALTER TABLE test_line ADD CONSTRAINT FK_D46D9E391E5D0459 FOREIGN KEY (test_id) REFERENCES test (id)');
        $this->addSql('ALTER TABLE test_line_answer ADD CONSTRAINT FK_F1EDA726913AA18A FOREIGN KEY (test_line_id) REFERENCES test_line (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_line_answer ADD CONSTRAINT FK_F1EDA726AA334807 FOREIGN KEY (answer_id) REFERENCES answer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1BA977936C FOREIGN KEY (tree_root) REFERENCES topic (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1B727ACA70 FOREIGN KEY (parent_id) REFERENCES topic (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1B3DA5256D FOREIGN KEY (image_id) REFERENCES media (id)');
        $this->addSql('ALTER TABLE url_resource ADD CONSTRAINT FK_DEF19A9FBF396750 FOREIGN KEY (id) REFERENCES resource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64986383B10 FOREIGN KEY (avatar_id) REFERENCES media (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE answer DROP FOREIGN KEY FK_DADD4A251E27F6BF');
        $this->addSql('ALTER TABLE cours_resource DROP FOREIGN KEY FK_7B055ACBBF396750');
        $this->addSql('ALTER TABLE file_resource DROP FOREIGN KEY FK_DC17B8BEA9FDD75');
        $this->addSql('ALTER TABLE file_resource DROP FOREIGN KEY FK_DC17B8BBF396750');
        $this->addSql('ALTER TABLE leitner_test DROP FOREIGN KEY FK_EECEE4F4BF396750');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494E1F55203D');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494E2FA1B98');
        $this->addSql('ALTER TABLE quizz DROP FOREIGN KEY FK_7C77973DBF396750');
        $this->addSql('ALTER TABLE resource DROP FOREIGN KEY FK_BC91F4161F55203D');
        $this->addSql('ALTER TABLE test DROP FOREIGN KEY FK_D87F7E0CA76ED395');
        $this->addSql('ALTER TABLE test_line DROP FOREIGN KEY FK_D46D9E391E27F6BF');
        $this->addSql('ALTER TABLE test_line DROP FOREIGN KEY FK_D46D9E391E5D0459');
        $this->addSql('ALTER TABLE test_line_answer DROP FOREIGN KEY FK_F1EDA726913AA18A');
        $this->addSql('ALTER TABLE test_line_answer DROP FOREIGN KEY FK_F1EDA726AA334807');
        $this->addSql('ALTER TABLE topic DROP FOREIGN KEY FK_9D40DE1BA977936C');
        $this->addSql('ALTER TABLE topic DROP FOREIGN KEY FK_9D40DE1B727ACA70');
        $this->addSql('ALTER TABLE topic DROP FOREIGN KEY FK_9D40DE1B3DA5256D');
        $this->addSql('ALTER TABLE url_resource DROP FOREIGN KEY FK_DEF19A9FBF396750');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64986383B10');
        $this->addSql('DROP TABLE answer');
        $this->addSql('DROP TABLE cours_resource');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE file_resource');
        $this->addSql('DROP TABLE leitner_test');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE quizz');
        $this->addSql('DROP TABLE resource');
        $this->addSql('DROP TABLE test');
        $this->addSql('DROP TABLE test_line');
        $this->addSql('DROP TABLE test_line_answer');
        $this->addSql('DROP TABLE topic');
        $this->addSql('DROP TABLE url_resource');
        $this->addSql('DROP TABLE user');
    }
}
