<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231213205851 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE question_resource (question_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', resource_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_DF99441A1E27F6BF (question_id), INDEX IDX_DF99441A89329D25 (resource_id), PRIMARY KEY(question_id, resource_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE question_resource ADD CONSTRAINT FK_DF99441A1E27F6BF FOREIGN KEY (question_id) REFERENCES question (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE question_resource ADD CONSTRAINT FK_DF99441A89329D25 FOREIGN KEY (resource_id) REFERENCES resource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494E2FA1B98');
        $this->addSql('DROP INDEX IDX_B6F7494E2FA1B98 ON question');
        $this->addSql('ALTER TABLE question ADD explanation LONGTEXT DEFAULT NULL, DROP resource_answer_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE question_resource DROP FOREIGN KEY FK_DF99441A1E27F6BF');
        $this->addSql('ALTER TABLE question_resource DROP FOREIGN KEY FK_DF99441A89329D25');
        $this->addSql('DROP TABLE question_resource');
        $this->addSql('ALTER TABLE question ADD resource_answer_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP explanation');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494E2FA1B98 FOREIGN KEY (resource_answer_id) REFERENCES resource (id)');
        $this->addSql('CREATE INDEX IDX_B6F7494E2FA1B98 ON question (resource_answer_id)');
    }
}
