<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231213205454 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE topic_resource (topic_id INT NOT NULL, resource_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_4AA079101F55203D (topic_id), INDEX IDX_4AA0791089329D25 (resource_id), PRIMARY KEY(topic_id, resource_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE topic_resource ADD CONSTRAINT FK_4AA079101F55203D FOREIGN KEY (topic_id) REFERENCES topic (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE topic_resource ADD CONSTRAINT FK_4AA0791089329D25 FOREIGN KEY (resource_id) REFERENCES resource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cours_resource ADD title VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE resource DROP FOREIGN KEY FK_BC91F4161F55203D');
        $this->addSql('DROP INDEX IDX_BC91F4161F55203D ON resource');
        $this->addSql('ALTER TABLE resource DROP topic_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE topic_resource DROP FOREIGN KEY FK_4AA079101F55203D');
        $this->addSql('ALTER TABLE topic_resource DROP FOREIGN KEY FK_4AA0791089329D25');
        $this->addSql('DROP TABLE topic_resource');
        $this->addSql('ALTER TABLE resource ADD topic_id INT NOT NULL');
        $this->addSql('ALTER TABLE resource ADD CONSTRAINT FK_BC91F4161F55203D FOREIGN KEY (topic_id) REFERENCES topic (id)');
        $this->addSql('CREATE INDEX IDX_BC91F4161F55203D ON resource (topic_id)');
        $this->addSql('ALTER TABLE cours_resource DROP title');
    }
}
