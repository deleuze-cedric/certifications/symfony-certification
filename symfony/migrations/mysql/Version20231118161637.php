<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231118161637 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE topic DROP FOREIGN KEY FK_9D40DE1BA977936C');
        $this->addSql('DROP INDEX IDX_9D40DE1BA977936C ON topic');
        $this->addSql('ALTER TABLE topic CHANGE tree_root root INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D40DE1B5E237E06727ACA70 ON topic (name, parent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D40DE1B625465C6727ACA70 ON topic (bundle_name, parent_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_9D40DE1B5E237E06727ACA70 ON topic');
        $this->addSql('DROP INDEX UNIQ_9D40DE1B625465C6727ACA70 ON topic');
        $this->addSql('ALTER TABLE topic CHANGE root tree_root INT DEFAULT NULL');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1BA977936C FOREIGN KEY (tree_root) REFERENCES topic (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9D40DE1BA977936C ON topic (tree_root)');
    }
}
