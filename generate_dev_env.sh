#!/bin/bash

echo '' > .env

echo "# Configuration utilisateur" >> .env
echo "USER_ID=$(id -u $USER)" >> .env
echo "GROUP_ID=$(id -g $USER)" >> .env
echo "" >> .env

echo "# Configuration git" >> .env
echo "GIT_EMAIL=$(git config user.email)" >> .env
echo "GIT_NAME=\"$(git config user.name)\"" >> .env
echo "" >> .env

KEY=$(openssl rand -base64 40)

echo "# Configuration mercure" >> .env
echo "MERCURE_PUBLISHER_JWT_KEY=\"$KEY\"" >> .env
echo "MERCURE_SUBSCRIBER_JWT_KEY=\"$KEY\"" >> .env
echo "" >> .env

echo "The .env file has been successfully generated"